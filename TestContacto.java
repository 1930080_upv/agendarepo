import java.util.Date;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

public class TestContacto 
{
    public static void main(String... args){
        String nombre;
        Date fdn;
        String email;
        long telefono;
        
                  
        //CONSTRUCTOR CON 4 PARAMETROS
        nombre = "Mafalda Gomez";
        fdn = new Date(100, 2, 17); // enero=> 0, febrero=> 1, marzo=> 2
        email = "mafalda@upv.edu.mx";
        telefono = 8343218965L;
        
        Contacto mafalda = new Contacto(nombre,fdn, email, telefono);
        System.out.println(mafalda.toString());
        
        FileOutputStream fos = null;
        ObjectOutputStream salida = null;
        
        try{
           fos = new FileOutputStream("data.ser");
           salida = new ObjectOutputStream(fos);
           
           salida.writeObject(mafalda);
        }
        
        catch(Exception e){
            e.printStackTrace();
        }
        
        finally{
            try{
               if (salida != null)
                  salida.close();
               if (fos != null)
                  fos.close();
            }
            catch(IOException e){
               e.printStackTrace();
            }
        }
        
        
        
    }
}
