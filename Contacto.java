import java.util.Date;
import java.io.Serializable;
public class Contacto extends Persona implements Serializable
{
     String email;
     long telefono;
    
     public Contacto(String a, Date b){
            super(a,b);
        }
        
     public Contacto(String a, String c, long d){
            super(a);
            this.email = c;
            this.telefono = d;
        }   
        
     public Contacto(String a, Date b, String c, long d){
            super(a,b);
            this.email = c;
            this.telefono = d;
        }
        
     public String getEmail(){
           return this.email;
        }
     public void setEmail(String a){
            this.email = a;
        }
        
     public long getTelefono(){
           return this.telefono;
        }
     public void setTelefono(long a){
            this.telefono = a;
     }
        
     public String toString(){
           return nombre + " - "+fdn+" - "+email+" - "+telefono;
        }
        
}
