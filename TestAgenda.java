import java.util.*;
public class TestAgenda
{
   public static void main(String... args){
        Scanner sc;
        sc = new Scanner(System.in);
        Scanner read;
        read = new Scanner(System.in);
        char op='0';
        int n =0;
        int num,c;
        boolean ban_1= false;
        
        String nombre;
        Date fdn;
        String email;
        long telefono;
        
        Agenda obj = new Agenda();
        Contacto contacto,tmp;
        tmp = null;
                
        //CONSTRUCTOR CON 4 PARAMETROS 
        /*nombre = "Mafalda Gomez";
        fdn = new Date(100, 2, 17); // enero=> 0, febrero=> 1, marzo=> 2
        email = "mafalda@upv.edu.mx";
        telefono = 8343218965L;
        contacto = new Contacto(nombre,fdn, email, telefono);
        obj.agregarContacto(contacto);
        
        nombre = "Felipillo Perez";
        fdn = new Date(100, 5, 18); // enero=> 0, febrero=> 1, marzo=> 2
        email = "felipilo@upv.edu.mx";
        telefono = 8341238578L;
        contacto = new Contacto(nombre,fdn, email, telefono);
        obj.agregarContacto(contacto);
        
        nombre = "Carlitos Schultz";
        fdn = new Date(100, 10, 10); // enero=> 0, febrero=> 1, marzo=> 2
        email = "carlitos@upv.edu.mx";
        telefono = 8348438279L;
        contacto = new Contacto(nombre,fdn, email, telefono);
        obj.agregarContacto(contacto);*/
       
        
        int nMax = obj.agenda.size() - 1; // numero indice para la agenda
        
        try{
              tmp = obj.buscarContacto(8343218965L);
              System.out.println(tmp.toString());
        }
        catch (NullPointerException e){
              System.out.println("No se econtro ese contacto");
        }
        catch(Exception e){
           e.printStackTrace();
        }
        // q-> arriba(hacia el primero)  w->abajo(hacia el ultimo contacto)
        try{
            System.out.println("Navegar en la Agenda");
            //System.out.println(obj.navegarAgenda(n).toString()); //MARCA ERROR
            System.out.println("q->Arriba, w->Abajo, s->Salir, b->Borrar, a->Agregar");
            while(true){
               op = sc.nextLine().trim().charAt(0);
               if( op == 's')
                  break;
               if(op == 'q'){
                   if (n == 0){
                      n = 0;
                      System.out.println(obj.navegarAgenda(n).toString() );
                    }
                    if (n >0 && n <= nMax ){
                       n = n -1;
                       System.out.println(obj.navegarAgenda(n).toString() );
                    }
                }
                if(op == 'w' ){
                   if (n == nMax){
                       n = nMax;//Es para enfasis, no es necesaria esta linea
                       System.out.println(obj.navegarAgenda(n).toString() );
                    }
                    if( n>=0 && n <nMax ){
                       n = n +1;
                       System.out.println(obj.navegarAgenda(n).toString() );
                    }
                }
                //ME MARCA ERRORES QUE NO PUDE SOLUCIONAR:(
                //NO PUDE VERIFICAR SI LO SIGUIENTE FUNCIONABA
                if(op == 'b' ){ //para borrar
                   System.out.println("Eliminando un contacto");
                   System.out.println("Ingrese el número del contacto a eliminar:");
                   num = read.nextInt();
                   c=obj.borrarContacto(num);
                   
                }
                
                if(op == 'a'){ //agregar a agenda
                   //Es para enfasis, no es necesaria esta linea
                   System.out.println("Agregando nuevo contacto");
                   System.out.println("Nombre: ");
                   nombre = sc.nextLine();
                   System.out.println("Email: ");
                   email = sc.nextLine();
                   System.out.println("Teléfono: ");
                   telefono =Long.valueOf(sc.nextLine());
                   contacto = new Contacto (nombre, email, telefono);
                   obj.agregarContacto(contacto);
                   ban_1 = obj.guardarAgenda();
                }
            }
            
        }
        catch(Exception e){
             e.printStackTrace();
        }
        
        finally{
            ban_1 =  obj.guardarAgenda();
            System.out.println("GUardando lista de contactos: "+ban_1);
            // Si muestra TRUE se guardo la lista a un archivo.
            //Si muestra FALSE hubo una excepcion y no se guardo la lista.
        }
    }
}
